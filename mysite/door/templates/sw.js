// Register event listener for the 'push' event.
var eventInfo;

//Envoit de la notification
self.addEventListener('push', function (event) {
    // Retrieve the textual payload from event.data (a PushMessageData object).
    // Other formats are supported (ArrayBuffer, Blob, JSON), check out the documentation
    // on https://developer.mozilla.org/en-US/docs/Web/API/PushMessageData.
    eventInfo = event.data.text();
    //Lecture des infos mis dans l'event
    const data = JSON.parse(eventInfo);
    //Si il y a des données dans data.head alors on entre dans head data.head, sinon l'autre ..
    const head = data.head || 'New Notification 🕺🕺';
    const body = data.body || 'This is default content. Your notification didn\'t have one 🙄🙄';
    const icon = data.icon || 'https://i.imgur.com/MZM3K5w.png';
    // Keep the service worker alive until the notification is created.
    event.waitUntil(
        self.registration.showNotification(head, {
	          body: body,
	          icon: icon
	      })
    );
});

//Ajout un event sur la notification, lorsqu'on clic cela permet d'ouvrire un lien.
self.addEventListener('notificationclick', function(event) {

    const data = JSON.parse(eventInfo);
    const url = data.url || "https://192.168.7.129/";

    
    console.log('[Service Worker] Notification click Received.');
    
    event.notification.close();
    
    event.waitUntil(
        clients.openWindow(url)
    );
});

