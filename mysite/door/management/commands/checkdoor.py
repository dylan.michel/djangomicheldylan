from django.core.management.base import BaseCommand, CommandError
from door.models import Door as Porte

class Command(BaseCommand):
    help = 'Permet la mise à jour de l\'état des portes dans la bases de donnée'

    def handle(self, *args, **options):
        import time
        import RPi.GPIO as GPIO
        #import socket

        try:
            ###VARIABLE###
            print ("Variable initialisation .")
            temps = time.time()
            ##Declaration de la liste qui va contenir les données portes
            alldoor = list()
            print ("Succesfull")
            ###INITIALISATION###
            print ("Parameter initialisation ..")
            ##Defini le type de définition des pins
            GPIO.setmode(GPIO.BCM)

            #Ajout des portes dans la listes via tout les objects inscrits dans la base de données
            for pin in Porte.objects.all():
                GPIO.setup(pin.pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
                if (GPIO.input(pin.pin) != pin.status):
                    pin.status = GPIO.input(pin.pin)
                    pin.save()
                alldoor.append(pin)
            print ("Succesfull")
        except:
            print("Initialisation Err ")
        else:
            def clean(self):
                try:
                    GPIO.cl1eanup()
                except:
                    print("Impossible de clean :/")
                finally:
                    print("Extinting..")
            #Permet de mettre en pause le programme pour éviter la consomation excecive du cpu
            def pause(self):
                time.sleep(0.5)
            
            def procedure(self):
                print ("Process start ...")
                while 1:
                    for porte in alldoor:
                        if GPIO.input(porte.pin) != porte.status:
                            if porte.status:
                                porte.status = False
                                porte.save()
                                print("Door closed : " + str(porte.pin))
                            else:
                                porte.status = True
                                porte.save()
                                print("Door opened : " + str(porte.pin))
                    pause(self)
                print ("End of process")
            #Debut du programme
            try:
                for pin in Porte.objects.all():
                    print(pin.pin)
                    
                procedure(self)
            
            
            except KeyboardInterrupt:
                print("Forced by keyboard")
            finally:
                clean(self)
