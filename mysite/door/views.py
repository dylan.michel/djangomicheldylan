#Load 
from .models import Door
from django.views import generic
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.contrib.auth.decorators import login_required

####PUSHWEB####
from django.http.response import JsonResponse, HttpResponse
from django.views.decorators.http import require_GET, require_POST
from django.contrib.auth.models import User, Group
from django.views.decorators.csrf import csrf_exempt
from webpush import send_user_notification, send_group_notification
import json
from django.conf import settings

# Create your views here.

class IndexView(generic.ListView):
    #Utilisation du fichier index.html
    template_name = 'door/index.html'
    #idk
    context_object_name = 'porte_list'

    #Retourne la liste des portes
    def get_queryset(self):
        return Door.objects.all()


@require_GET
def sub(request):
    
    webpush_settings = getattr(settings, 'WEBPUSH_SETTINGS', {})
    vapid_key = webpush_settings.get('VAPID_PUBLIC_KEY')
    user = request.user
    list_user = User.objects.all()
    userselected = request.GET.get('userselected', False)
    if userselected:
        return render(request, 'sub.html', {'user': user, 'vapid_key': vapid_key, 'list_user' : list_user, 'userselected': userselected})
    else:
        return render(request, 'sub.html', {'user': user, 'vapid_key': vapid_key, 'list_user' : list_user})

#@login_required
@require_POST
@csrf_exempt
def send_push(request):
    try:
        body = request.body
        #Sert à capturer les données du l'utilisateur, decode('utf-8') sert à rendre compatible les formats 
        data = json.loads(body.decode('utf-8'))
        #Vérification de la présence de donnée dans le data  => head body id
        if 'head' not in data or 'body' not in data or 'id' not in data:
            #Si l'un des 3 n'est pas presents alors on retourne une erreur
            return JsonResponse(status=400, data={"message": "Invalid data format"})
        #Prend l'info du premiers groupe crée
        #group = get_object_or_404(Group, pk= 1 )
        
        #Temps de sauvegarde de la notifcation en seconde
        ttl = 500

        #Si l'urlpush n'est pas dans data alors, la notification provient de sub sinon c'est de l'index
        if 'urlpush' not in data:
            #Recupère l'utilisateur dans la base de donnée indiquer dans le data id
            user = get_object_or_404(User, username = data['id'])
            #Charge le payload pour l'envoie de donnée
            payload = {"head": data["head"], "body": data["body"]}
        else:
            #Envois de la notification à l'utilisateur 1, soit l'admin
            user_id = 1
            #Récupère l'utilisateur user_id dans la base de données
            user = get_object_or_404(User, pk=user_id)
            #Charge le payload avec toutes les infos d'envoies
            payload = {"head": data["head"], "body": data["body"], "icon": data["icons"] ,"url": data["urlpush"]}

        #Envoit de la notification (Utilisateur, les données de la notifcation, le temps de vie)
        send_user_notification(user, payload, ttl)
        
        #Teste de 'l'envoie de notif de groupe si il y a plusieurs personne qui gère le poste
        #send_group_notification(group, payload, ttl)

        #La notification a bien été envoyé
        return JsonResponse(status=200, data={"message": "Web push successful"})
    except TypeError:
        #Une erreur est survenu
        return JsonResponse(status=500, data={"message": "L'exception ;("})


#Méthode qui permet d'envoyer le status de la porte lorsqu'on l'appel, 
@require_POST
@csrf_exempt
def ajax_door(request):
    try:
        #Chargement du body
        body = request.body
        #Décodage du body
        data = json.loads(body.decode('utf-8'))
        #Recuperation du tuple door pour la porte spécifier
        porte_id = get_object_or_404(Door, door_name = data['adoor'])
        #Retourne la donnée avec le status de la porte
        return JsonResponse(status=200, data={"message": porte_id.status})
    except TypeError:
        #Une erreur est survenue
        return JsonResponse(status=500, data={"message": "Ajax door exception"})
