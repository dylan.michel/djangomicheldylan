from django.urls import path, include
from . import views
from .views import send_push, sub, ajax_door
from django.views.generic import TemplateView

app_name = 'door'
urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('sub', sub),
    path('send_push', send_push),
    path('ajax_door', ajax_door),
    path('webpush/', include('webpush.urls')),
    path('sub/sw.js', TemplateView.as_view(template_name='sw.js', content_type='application/x-javascript'))
]
