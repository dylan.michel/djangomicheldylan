from django.db import models
import datetime
from django.utils import timezone

# Create your models here.

class Door(models.Model):
    door_name = models.CharField(max_length=10)
    pin = models.IntegerField(primary_key = True)
    status = models.BooleanField(default = False)
    def __str__(self):
        return self.door_name

    def changement_status(self, boolean):
        status = boolean
        return self.status
