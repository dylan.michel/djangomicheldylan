from django.contrib import admin

# Register your models here.

from .models import Door

admin.site.register(Door)
