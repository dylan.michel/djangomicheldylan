const pushForm = document.getElementById('send-push__form');
const errorMsg = document.querySelector('.error');

pushForm.addEventListener('submit', async function (e) {
    e.preventDefault();

    //Sur le première element de send-push__form..
    const input = this[0];
    //deuxieme et ect
    const select = this[1];
    const textarea = this[2];
    const button = this[3];

    //Mise a vide de la zone d'erreur
    errorMsg.innerText = '';

    //Recupère les valeurs dans les zones de textes
    const head = input.value;
    const body = textarea.value;

    //Recupère l'utilisateur selectionner
    const id = select.options[select.selectedIndex].text;

    //Si aucun champs est vide et l'utilisateur selectionné alors on envoit
    if (head && body && id) {
        button.innerText = 'Envoie en cour...';
        button.disabled = true;

        //Utilisation de la vue send_push
        const res = await fetch('/send_push', {
            method: 'POST',
            body: JSON.stringify({head, body, id}),
            headers: {
                'content-type': 'application/json'
            }
        });

        //Si la valeur de réponse est 200 alors on peut en envoyer une autre..
        if (res.status === 200) {
            button.innerText = 'Envoyer';
            button.disabled = false;
            input.value = '';
            textarea.value = '';
        } else {
            errorMsg.innerText = res.message;
            button.innerText = "Quelquechose ne s'est pas passé comme prévu :/. Ressayé";
            button.disabled = false
        }
    }
    //Si des erreures surviennent
    else {
        let error;
        //Si head ou body n'est pas remplis
        if (!head || !body){
            error = "Completer tout les champs disponibles !"
        }
        //Si la personne n'est pas connecté
        else if (!id){
            error = "Vous n'êtes pas connecter."
        }
        errorMsg.innerText = error;
    }    
});
