//Recuperation de la zone de texte para
text = document.getElementById("para");

//Recupère la zone de texte pour les erreurs
errorMsg = document.querySelector(".error");  //querySelector

//Recupère la liste des boutons nommées buttondoor
var button = document.getElementsByName("buttondoor");

//Fonction qui traite l'envoie de la notifcation
// a Bouton qui est pressé
async function fonc(a)
{
    Button(true);
    //Element de teste
    text.innerHTML = "";                                       
    errorMsg.innerText = "";
    //Recuperation de données....
    const meta = document.querySelector('meta[name="user_id"]');
    const id = meta ? meta.content : null;
    const metaname= document.querySelector('meta[name="username"]');
    const name = metaname ? metaname.content : null;
    const head = ("Demande de fermeture");
    const body = `La porte ${a} est demandé à être fermé par ${name}`;
    const urlpush = `https://192.168.7.129/sub?userselected=${name}`;
    const icons = `https://i.imgur.com/dRDxiCQ.png`;
    const adoor = a;

    //Envois de la requête pour recuperer l'état de la porte
    const resdoor = await fetch('/ajax_door',
                                {
                                    method: 'POST',
                                    body: JSON.stringify({adoor}),
                                    headers:
                                    {
                                        'content-type': 'application/json'
                                    }
                                }).then(res => res.json());//Rend resdoor utilisable pour lire les messages

    //const datadoor = resdoor.json();
    //const adatadoor = JSON.parse(resdoor);
    //const adataboor = datadoor.message[0];
    //Si la porte est fermée, alors 
    if(!resdoor.message)
    {

        //Si id est présent//soit l'utilisateur connecté alors envois de la notification
        if (id) {
            //Affichage d'un message dans une zone texte
            text.innerHTML = 'Sending...';
            //Desactivation des boutons
            //Envois de la notification
            const res = await fetch('/send_push', {
                method: 'POST',
                body: JSON.stringify({head, body, id, icons, urlpush}),
                headers: {
                    'content-type': 'application/json'
                }
            });
            //Si la reponse est 200 alors c'est bien envoyer,sinon un probleme est survenu
            if (res.status === 200) {
                text.innerText = 'La notification à bien été envoyée !';
                
            } else {
                text.innerText = "Ca n'a pas fonctionnée";
                errorMsg.innerText = res.message;
                
            }
        }
        //Erreur de non connection
        else {
            let error;
            if (!id){
                error = "Es-tu connectée ? Vérifie !";
                errorMsg.innerText = error;
            }}
    }
    //Erreur la porte n'est pas fermée
    else
    {
        error = "La porte n'est pas fermée !";
        errorMsg.innerText = error;
    }
    Button(false);
};

//Fonction pour desactiver les boutons des portes
function Button(bool)
{
    button.forEach(function(abut)
                   {
                       abut.disabled = bool;
                   }
                  );
};
