DJANGO_VENV=../django-venv
SERVER_PORT=8000
SERVER_ADDRESS=0.0.0.0

.PHONY: all venv install-pip-packages install run

all: run quit-venv

venv: $(DJANGO_VENV)

$(DJANGO_VENV):
	python3 -m venv $(DJANGO_VENV)

install: .install
.install: install-pip-packages
	@touch .install

install-pip-packages: .install-pip-packages
.ONESHELL:
.install-pip-packages: $(DJANGO_VENV)
	. $(DJANGO_VENV)/bin/activate
	pip3 install wheel
	pip3 install -r requirements.txt
	deactivate
	touch .install-pip-packages

.ONESHELL:
run: install
	. $(DJANGO_VENV)/bin/activate
	-mysite/manage.py runserver $(SERVER_ADDRESS):$(SERVER_PORT)
	deactivate

clean:
	-rm -rf $(DJANGO_VENV)
	-rm .install .install-pip-packages
